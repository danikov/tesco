package com.tesco;

import org.junit.Test;

import static com.tesco.Accumulation.Coin;
import static org.junit.Assert.*;

/**
 * Tests for {@link VendingMachine}
 */
public class VendingMachineTest {

    @Test
    public void defaultStateIsOff() {
        VendingMachine machine = new VendingMachine();
        assertFalse(machine.isOn());
    }

    @Test
    public void turnsOn() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();
        assertTrue(machine.isOn());
    }

    @Test
    public void turnsOff() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();
        machine.setOff();
        assertFalse(machine.isOn());
    }

    @Test
    public void acceptsTenPence() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();
        machine.insertCoin(Coin.TEN);

        assertEquals(1, machine.getCoinCount(Coin.TEN));
        assertEquals(10, machine.getBalance());
    }

    @Test
    public void acceptsTwentyPence() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();
        machine.insertCoin(Coin.TWENTY);

        assertEquals(1, machine.getCoinCount(Coin.TWENTY));
        assertEquals(20, machine.getBalance());
    }

    @Test
    public void acceptsFiftyPence() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();
        machine.insertCoin(Coin.FIFTY);

        assertEquals(1, machine.getCoinCount(Coin.FIFTY));
        assertEquals(50, machine.getBalance());
    }

    @Test
    public void acceptsPound() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();
        machine.insertCoin(Coin.POUND);

        assertEquals(1, machine.getCoinCount(Coin.POUND));
        assertEquals(100, machine.getBalance());
    }

    @Test
    public void rejectsCoinsWhenOff() {
        VendingMachine machine = new VendingMachine();
        assertFalse(machine.isOn());

        machine.insertCoin(Coin.TEN);
        machine.insertCoin(Coin.TWENTY);
        machine.insertCoin(Coin.FIFTY);
        machine.insertCoin(Coin.POUND);

        assertEquals(0, machine.getCoinCount(Coin.TEN));
        assertEquals(0, machine.getCoinCount(Coin.TWENTY));
        assertEquals(0, machine.getCoinCount(Coin.FIFTY));
        assertEquals(0, machine.getCoinCount(Coin.POUND));
        assertEquals(0, machine.getBalance());
    }

    @Test
    public void returnsSingleCoin() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();
        machine.insertCoin(Coin.TEN);
        Accumulation coins = machine.returnCoins();

        assertEquals(1, coins.count());
        assertTrue(coins.contains(Coin.TEN));
        assertEquals(0, machine.getBalance());
    }

    @Test
    public void returnsDifferentCoins() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();
        machine.insertCoin(Coin.TEN);
        machine.insertCoin(Coin.TWENTY);
        machine.insertCoin(Coin.FIFTY);
        machine.insertCoin(Coin.POUND);
        Accumulation coins = machine.returnCoins();

        assertEquals(4, coins.count());
        assertTrue(coins.contains(Coin.POUND));
        assertTrue(coins.contains(Coin.FIFTY));
        assertTrue(coins.contains(Coin.TWENTY));
        assertTrue(coins.contains(Coin.TEN));
        assertEquals(0, machine.getBalance());
    }

    @Test
    public void returnsMultiplesOfCoins() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();
        machine.insertCoin(Coin.TEN);
        machine.insertCoin(Coin.TEN);
        machine.insertCoin(Coin.TEN);
        machine.insertCoin(Coin.TEN);
        Accumulation coins = machine.returnCoins();

        assertEquals(4, coins.count());
        assertEquals(40, coins.balance());
        assertEquals(4, coins.count(Coin.TEN));
        assertEquals(0, machine.getBalance());
    }

    @Test
    public void returnsNothingWhenOff() {
        VendingMachine machine = new VendingMachine();
        assertFalse(machine.isOn());

        machine.insertCoin(Coin.TEN);
        machine.insertCoin(Coin.TWENTY);
        machine.insertCoin(Coin.FIFTY);
        machine.insertCoin(Coin.POUND);
        Accumulation coins = machine.returnCoins();

        assertEquals(0, coins.count());
        assertEquals(0, machine.getBalance());
    }

    @Test
    public void vendsItemA() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();

        machine.insertCoin(Coin.FIFTY);
        machine.insertCoin(Coin.TEN);
        assertEquals(60, machine.getBalance());

        assertEquals("A", machine.vendItem("A"));
        assertEquals(0, machine.getBalance());
    }

    @Test
    public void vendsItemB() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();

        machine.insertCoin(Coin.POUND);
        assertEquals(100, machine.getBalance());

        assertEquals("B", machine.vendItem("B"));
        assertEquals(0, machine.getBalance());
    }

    @Test
    public void vendsItemC() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();

        machine.insertCoin(Coin.POUND);
        machine.insertCoin(Coin.FIFTY);
        machine.insertCoin(Coin.TWENTY);
        assertEquals(170, machine.getBalance());

        assertEquals("C", machine.vendItem("C"));
        assertEquals(0, machine.getBalance());
    }

    @Test
    public void doesNotVendUnknownItems() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();

        machine.insertCoin(Coin.POUND);
        assertEquals(100, machine.getBalance());

        assertEquals("Item not found", machine.vendItem("D"));
        assertEquals(100, machine.getBalance());
    }

    @Test
    public void doesNotVendWhenOff() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();

        machine.insertCoin(Coin.FIFTY);
        machine.insertCoin(Coin.TEN);
        assertEquals(60, machine.getBalance());
        machine.setOff();
        assertFalse(machine.isOn());

        assertEquals("Power Off", machine.vendItem("A"));
        assertEquals(60, machine.getBalance());
    }

    @Test
    public void rejectsInsufficientFunds() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();

        machine.insertCoin(Coin.FIFTY);
        assertEquals(50, machine.getBalance());

        assertEquals("Insufficient funds", machine.vendItem("A"));
        assertEquals(50, machine.getBalance());

        machine.insertCoin(Coin.TWENTY);
        machine.insertCoin(Coin.TWENTY);
        assertEquals(90, machine.getBalance());

        assertEquals("Insufficient funds", machine.vendItem("B"));
        assertEquals(90, machine.getBalance());

        machine.insertCoin(Coin.FIFTY);
        machine.insertCoin(Coin.TWENTY);
        assertEquals(160, machine.getBalance());

        assertEquals("Insufficient funds", machine.vendItem("C"));
        assertEquals(160, machine.getBalance());
    }

    @Test
    public void returnsChangeWhenRequested() {
        VendingMachine machine = new VendingMachine();
        machine.stockCoins(100, Coin.TEN);
        machine.stockCoins(100, Coin.TWENTY);
        machine.stockCoins(100, Coin.FIFTY);
        machine.stockCoins(100, Coin.POUND);
        machine.setOn();

        machine.insertCoin(Coin.POUND);
        assertEquals(100, machine.getBalance());

        assertEquals("A", machine.vendItem("A"));
        assertEquals(40, machine.getBalance());

        Accumulation coins = machine.returnCoins();
        assertEquals(40, coins.balance());
        assertEquals(2, coins.count(Coin.TWENTY));
        assertEquals(0, machine.getBalance());
    }

    @Test
    public void doesNotReturnChangeWithInsufficientCoins() {
        VendingMachine machine = new VendingMachine();
        machine.stockCoins(100, Coin.FIFTY);
        machine.stockCoins(100, Coin.POUND);
        machine.setOn();

        machine.insertCoin(Coin.POUND);
        assertEquals(100, machine.getBalance());

        assertEquals("A", machine.vendItem("A"));
        assertEquals(40, machine.getBalance());

        Accumulation coins = machine.returnCoins();
        assertEquals(0, coins.balance());
        assertEquals(0, coins.count());
        assertEquals(40, machine.getBalance());
    }

    @Test
    public void returnPartialChangeWithInsufficientCoins() {
        VendingMachine machine = new VendingMachine();
        machine.stockCoins(100, Coin.FIFTY);
        machine.stockCoins(100, Coin.POUND);
        machine.setOn();

        machine.insertCoin(Coin.POUND);
        machine.insertCoin(Coin.TWENTY);
        assertEquals(120, machine.getBalance());

        assertEquals("A", machine.vendItem("A"));
        assertEquals(60, machine.getBalance());

        Accumulation coins = machine.returnCoins();
        assertEquals(50, coins.balance());
        assertEquals(1, coins.count(Coin.FIFTY));
        assertEquals(10, machine.getBalance());
    }

    @Test
    public void returnsCorrectComplexChange() {
        VendingMachine machine = new VendingMachine();
        machine.stockCoins(100, Coin.TEN);
        machine.stockCoins(100, Coin.TWENTY);
        machine.stockCoins(100, Coin.FIFTY);
        machine.stockCoins(100, Coin.POUND);
        machine.setOn();

        machine.insertCoin(Coin.POUND);
        machine.insertCoin(Coin.POUND);
        machine.insertCoin(Coin.FIFTY);
        assertEquals(250, machine.getBalance());

        assertEquals("A", machine.vendItem("A"));
        assertEquals(190, machine.getBalance());

        Accumulation coins = machine.returnCoins();
        assertEquals(190, coins.balance());
        assertEquals(1, coins.count(Coin.POUND));
        assertEquals(1, coins.count(Coin.FIFTY));
        assertEquals(2, coins.count(Coin.TWENTY));
        assertEquals(0, machine.getBalance());

        machine.insertCoin(Coin.POUND);
        machine.insertCoin(Coin.POUND);
        machine.insertCoin(Coin.TEN);
        assertEquals(210, machine.getBalance());

        assertEquals("A", machine.vendItem("A"));
        assertEquals(150, machine.getBalance());

        coins = machine.returnCoins();
        assertEquals(150, coins.balance());
        assertEquals(1, coins.count(Coin.POUND));
        assertEquals(1, coins.count(Coin.FIFTY));
        assertEquals(0, machine.getBalance());
    }
}
