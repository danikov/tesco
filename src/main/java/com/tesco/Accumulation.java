package com.tesco;

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import java.util.*;
import java.util.stream.Collectors;

public class Accumulation {
    enum Coin {
        TEN(10),
        TWENTY(20),
        FIFTY(50),
        POUND(100);

        final int value;

        Coin(int value) {
            this.value = value;
        }
    }

    private final Map<Coin, Integer> counts;

    public static class AccumulationBuilder {
        private int ten = 0;
        private int twenty = 0;
        private int fifty = 0;
        private int pound = 0;

        public AccumulationBuilder() {
        }

        public AccumulationBuilder(Accumulation copy) {
            this.ten = copy.counts.get(Coin.TEN);
            this.twenty = copy.counts.get(Coin.TWENTY);
            this.fifty = copy.counts.get(Coin.FIFTY);
            this.pound = copy.counts.get(Coin.POUND);
        }

        public AccumulationBuilder with(List<Coin> coins) {
            for (Coin coin : coins) {
                switch (coin) {
                    case TEN:
                        ten++;
                        break;
                    case TWENTY:
                        twenty++;
                        break;
                    case FIFTY:
                        fifty++;
                        break;
                    case POUND:
                        pound++;
                        break;
                }
            }

            return this;
        }

        public AccumulationBuilder with(int count, Coin coin) {
            switch (coin) {
                case TEN:
                    return withTens(count);
                case TWENTY:
                    return withTwenties(count);
                case FIFTY:
                    return withFifties(count);
                case POUND:
                    return withPounds(count);
            }

            return this;
        }

        public AccumulationBuilder withTens(int count) {
            ten = count;
            return this;
        }

        public AccumulationBuilder withTwenties(int count) {
            twenty = count;
            return this;
        }

        public AccumulationBuilder withFifties(int count) {
            fifty = count;
            return this;
        }

        public AccumulationBuilder withPounds(int count) {
            pound = count;
            return this;
        }

        public Accumulation build() {
            return new Accumulation(ten, twenty, fifty, pound);
        }
    }

    public Accumulation() {
        this(0, 0, 0, 0);
    }

    private Accumulation(int tens, int twenties, int fifties, int pounds) {
        counts = ImmutableMap.<Coin, Integer>builder()
                .put(Coin.TEN, tens)
                .put(Coin.TWENTY, twenties)
                .put(Coin.FIFTY, fifties)
                .put(Coin.POUND, pounds)
                .build();
    }

    public boolean contains(Coin coin) {
        return counts.get(coin) > 0;
    }

    public int balance() {
        return (Coin.TEN.value * counts.get(Coin.TEN)) +
                (Coin.TWENTY.value * counts.get(Coin.TWENTY)) +
                (Coin.FIFTY.value * counts.get(Coin.FIFTY)) +
                (Coin.POUND.value * counts.get(Coin.POUND));
    }

    public int count(Coin coin) {
        return counts.get(coin);
    }

    public int count() {
        int total = 0;
        for (Coin coin : Coin.values()) {
            total += counts.get(coin);
        }
        return total;
    }

    public Accumulation add(Coin coin) {
        return new AccumulationBuilder(this).with(counts.get(coin) + 1, coin).build();
    }

    /**
     * @return an attempt to remove the balance from this accumulation or as close as possible with the coins on hand
     */
    public Split splitBalance(int balance) {
        Collection<Accumulation> permutations = permutationsFor(balance);
        List<Accumulation> candidatePermutations = new ArrayList<>();
        int bestBalance = 0;

        for (Accumulation permutation : permutations) {
            if (permutation.balance() >= bestBalance && permutation.balance() <= balance) {
                if (permutation.balance() != bestBalance) {
                    bestBalance = permutation.balance();
                    candidatePermutations = new ArrayList<>();
                }
                candidatePermutations.add(permutation);
            }
        }

        if (candidatePermutations.size() == 0) {
            return new Split(this, 0, 0, 0, 0);
        }

        List<Accumulation> mostPoundAccumulations = mostCoins(Coin.POUND, candidatePermutations);

        if (mostPoundAccumulations.size() == 1) {
            return new Split(this, mostPoundAccumulations.get(0));
        }

        List<Accumulation> mostFiftyAccumulations = mostCoins(Coin.FIFTY, mostPoundAccumulations);

        if (mostFiftyAccumulations.size() == 1) {
            return new Split(this, mostFiftyAccumulations.get(0));
        }

        List<Accumulation> mostTwentyAccumulations = mostCoins(Coin.TWENTY, mostFiftyAccumulations);

        return new Split(this, mostTwentyAccumulations.get(0));
    }

    /**
     * @return all accumulations with the most of a given coin
     */
    private List<Accumulation> mostCoins(Coin coin, List<Accumulation> accumulations) {
        if(accumulations.size() == 0) {
            return accumulations;
        }

        int maxOfCoin = accumulations.stream()
                .max(Comparator.comparing(a -> a.count(coin)))
                .get().count(coin);

        return accumulations.stream()
                .filter(a -> a.count(coin) == maxOfCoin)
                .collect(Collectors.toList());
    }

    /**
     * @return permutations of available coins that are less than or equal to target balance
     */
    private Collection<Accumulation> permutationsFor(int balance) {
        List<Accumulation> permutations = Lists.newArrayList();
        List<List<Coin>> multiplesOfTen = Lists.newArrayList();
        List<List<Coin>> multiplesOfTwenty = Lists.newArrayList();
        List<List<Coin>> multiplesOfFifty = Lists.newArrayList();
        List<List<Coin>> multiplesOfPound = Lists.newArrayList();

        for (int i = 0; i <= Math.min(balance / 10, counts.get(Coin.TEN)); i++) {
            multiplesOfTen.add(Collections.nCopies(i, Coin.TEN));
        }
        for (int i = 0; i <= Math.min(balance / 20, counts.get(Coin.TWENTY)); i++) {
            multiplesOfTwenty.add(Collections.nCopies(i, Coin.TWENTY));
        }
        for (int i = 0; i <= Math.min(balance / 50, counts.get(Coin.FIFTY)); i++) {
            multiplesOfFifty.add(Collections.nCopies(i, Coin.FIFTY));
        }
        for (int i = 0; i <= Math.min(balance / 100, counts.get(Coin.POUND)); i++) {
            multiplesOfPound.add(Collections.nCopies(i, Coin.POUND));
        }

        for (List<Coin> tenList : multiplesOfTen) {
            for (List<Coin> twentyList : multiplesOfTwenty) {
                for (List<Coin> fiftyList : multiplesOfFifty) {
                    for (List<Coin> poundList : multiplesOfPound) {
                        List<Coin> permutation = Lists.newArrayList();
                        permutation.addAll(tenList);
                        permutation.addAll(twentyList);
                        permutation.addAll(fiftyList);
                        permutation.addAll(poundList);
                        Accumulation accumulation = new AccumulationBuilder().with(permutation).build();
                        if (accumulation.balance() <= balance) {
                            permutations.add(accumulation);
                        }
                    }
                }
            }
        }

        return permutations;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final Accumulation other = (Accumulation) obj;
        return Objects.equals(this.counts, other.counts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.counts);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("Total(p)", balance())
                .add("£1", count(Coin.POUND))
                .add("50p", count(Coin.FIFTY))
                .add("20p", count(Coin.TWENTY))
                .add("10p", count(Coin.TEN))
                .toString();
    }
}
