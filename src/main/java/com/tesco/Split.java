package com.tesco;

/**
 * Encapsulates a pair of Accumulations that have been split, origin being the original and remainder the result of
 * the subtraction
 */
class Split {
    final Accumulation origin, remainder;

    Split(Accumulation original, Accumulation subtraction) {
        this(original, subtraction.count(Accumulation.Coin.TEN), subtraction.count(Accumulation.Coin.TWENTY),
                subtraction.count(Accumulation.Coin.FIFTY), subtraction.count(Accumulation.Coin.POUND));
    }

    Split(Accumulation original, int tens, int twenties, int fifties, int pounds) {
        Accumulation.AccumulationBuilder originBuilder = new Accumulation.AccumulationBuilder(original);
        Accumulation.AccumulationBuilder remainderBuilder = new Accumulation.AccumulationBuilder();

        if (original.count(Accumulation.Coin.TEN) < tens) {
            originBuilder.withTens(0);
            remainderBuilder.withTens(original.count(Accumulation.Coin.TEN));
        } else {
            originBuilder.withTens(original.count(Accumulation.Coin.TEN) - tens);
            remainderBuilder.withTens(tens);
        }
        if (original.count(Accumulation.Coin.TWENTY) < twenties) {
            originBuilder.withTwenties(0);
            remainderBuilder.withTwenties(original.count(Accumulation.Coin.TWENTY));
        } else {
            originBuilder.withTwenties(original.count(Accumulation.Coin.TWENTY) - twenties);
            remainderBuilder.withTwenties(twenties);
        }
        if (original.count(Accumulation.Coin.FIFTY) < fifties) {
            originBuilder.withFifties(0);
            remainderBuilder.withFifties(original.count(Accumulation.Coin.FIFTY));
        } else {
            originBuilder.withFifties(original.count(Accumulation.Coin.FIFTY) - fifties);
            remainderBuilder.withFifties(fifties);
        }
        if (original.count(Accumulation.Coin.POUND) < pounds) {
            originBuilder.withPounds(0);
            remainderBuilder.withPounds(original.count(Accumulation.Coin.POUND));
        } else {
            originBuilder.withPounds(original.count(Accumulation.Coin.POUND) - pounds);
            remainderBuilder.withPounds(pounds);
        }

        origin = originBuilder.build();
        remainder = remainderBuilder.build();
    }
}
