package com.tesco;

import static com.tesco.Accumulation.Coin;

/**
 * Encapsulates the state of a vending machine and the operations that can be performed on it
 *
 * When powered off, the vending machine will retain all previous state but refuse to perform actions.
 *
 * As it's more interesting to implement rather than user-friendly, the machine will also return coins in a best-effort
 * manner when it has insufficient coins in store rather than refusing to vend when it cannot give exact change.
 */
public class VendingMachine {

    private boolean powered;
    private Accumulation coins;
    private int balance;

    public VendingMachine() {
        this.powered = false;
        this.coins = new Accumulation();
    }

    public boolean isOn() {
        return powered;
    }

    public void setOn() {
        powered = true;
    }

    public void setOff() {
        powered = false;
    }

    public void insertCoin(Coin coin) {
        if (powered) {
            coins = coins.add(coin);
            balance += coin.value;
        }
    }

    public int getCoinCount(Coin coin) {
        return coins.count(coin);
    }

    public int getBalance() {
        return balance;
    }

    /**
        @return an Accumulation of coins that would be ejected, empty if powered off
     */
    public Accumulation returnCoins() {
        if(!powered) {
            return new Accumulation();
        }
        Split split = coins.splitBalance(balance);
        coins = split.origin;
        balance = balance - split.remainder.balance();
        return split.remainder;
    }

    /**
     * @return the name of the item vended, or an error message if it cannot be vended
     */
    public String vendItem(String name) {
        if (!powered) {
            return "Power Off";
        }

        int value;
        switch (name) {
            case "A":
                value = 60;
                break;
            case "B":
                value = 100;
                break;
            case "C":
                value = 170;
                break;
            default:
                return "Item not found";
        }

        if (value > balance) {
            return "Insufficient funds";
        } else {
            balance -= value;
            return name;
        }
    }

    /**
     * Used to register coins stocked for change-providing purposes
     */
    public void stockCoins(int count, Coin coin) {
        coins = new Accumulation.AccumulationBuilder(coins).with(count, coin).build();
    }
}
